find_path(PTHREAD_INCLUDE_DIR
    pthread.h
    )
set(PTHREAD_INCLUDE_DIR ${PTHREAD_INCLUDE_DIR})

find_library(PTHREAD_RELEASE_LIBRARY
  NAMES
    pthread
    libpthread
    )
if (EXISTS ${PTHREAD_RELEASE_LIBRARY})
  list(APPEND PTHREAD_LIBRARIES optimized ${PTHREAD_RELEASE_LIBRARY})
endif()
find_library(PTHREAD_DEBUG_LIBRARY
  NAMES
    pthreadd
    libpthreadd
    )
if (EXISTS ${PTHREAD_DEBUG_LIBRARY})
  list(APPEND PTHREAD_LIBRARIES debug ${PTHREAD_DEBUG_LIBRARY})
endif()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(PTHREAD
  REQUIRED_VARS
    PTHREAD_INCLUDE_DIR
    PTHREAD_LIBRARIES)

mark_as_advanced(
  PTHREAD_INCLUDE_DIR
  PTHREAD_DEBUG_LIBRARY
  PTHREAD_RELEASE_LIBRARY)

if(PTHREAD_FOUND AND NOT TARGET Threads::Threads)
  add_library(Threads::Threads INTERFACE IMPORTED)
  target_include_directories(Threads::Threads INTERFACE "${PTHREAD_INCLUDE_DIR}")
  if (EXISTS ${PTHREAD_DEBUG_LIBRARY})
    target_link_libraries(Threads::Threads INTERFACE debug ${PTHREAD_DEBUG_LIBRARY})
  endif()
  if (EXISTS ${PTHREAD_RELEASE_LIBRARY})
    target_link_libraries(Threads::Threads INTERFACE optimized ${PTHREAD_RELEASE_LIBRARY})
  endif()
endif()
